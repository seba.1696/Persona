/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase direccion de la persona
 *
 * @author Sebastian Gonzalez
 */
public class Direccion {

    public String direccion;

    Direccion() {
    }

    Direccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que comprueba la valides de la cadena
     *
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Caracter(String str) {
        Pattern pat = Pattern.compile("^(([A-Za-z]+)*||((([A-Za-z]+)*\\s)*(\\#(\\d+)*)))+$");//Expresion regular del caso
        Matcher mat = pat.matcher(str);
        return mat.matches();
    }

    /**
     * Metodo que comprueba que el largo sea valido
     *
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Largo(String str) {
        return str.length() > 8 && str.length() <= 30;
    }

    /**
     * Metodo que muestra el resultado final de la verificacion
     *
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Result(String str) {
        if (Caracter(str)) {
            return Largo(str);
        } else {
            return false;
        }
    }
}
