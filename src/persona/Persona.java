/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 * Clase persona utilizada para realizar pruebas
 * @author Sebastian Gonzalez
 */
public class Persona {

    /**
     * Metodo main del programa
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Nombre nombre = new Nombre();
        System.out.println(nombre.Result("seba"));
        Telefono fono = new Telefono();
        System.out.println(fono.Result("56995171822"));
        Email email = new Email();
        System.out.println(email.Result("s.gonzalez11@ufromail.cl"));
        Direccion addr = new Direccion();
        System.out.println(addr.Result("Las marantas #02371"));
    }
    
}
