/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase telefono de la persona
 * @author Sebastian Gonzalez
 */
public class Telefono {

    public String fono;

    /**
     * Metodo constructor sin parametros
     */
    Telefono() {
    }

     /**
     * Metodo constructor con parametro
     * @param nombre Parametro nombre de la persona
     */
    Telefono(String fono) {
        this.fono = fono;
    }

    /**
     * Metodo que comprueba la valides de la cadena
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Caracter(String str) {
        Pattern pat = Pattern.compile("^([0-9])+$");//Expresion regular del caso
        Matcher mat = pat.matcher(str);
        return mat.matches();
    }

    public boolean Inicio(String str) {
        Pattern pat = Pattern.compile("^56.*");//Expresion regular del caso
        Matcher mat = pat.matcher(str);
        return mat.matches();
    }

    /**
     * Metodo que comprueba que el largo sea valido
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Largo(String str) {
        return str.length() == 11;
    }

    /**
     * Metodo que muestra el resultado final de la verificacion
     *
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Result(String str) {
        if (Caracter(str)) {
            if (Inicio(str)) {
                return Largo(str);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
