/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase email de la persona
 *
 * @author Sebastian Gonzalez
 */
public class Email {

    public String email;

    /**
     * Constructor sin parametros
     */
    Email() {
    }

    /**
     * Metodo constructor con parametro
     *
     * @param nombre Parametro email de la persona
     */
    Email(String email) {
        this.email = email;
    }

    /**
     * Metodo que comprueba la valides de la cadena
     *
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Caracter(String str) {
        //Expresion regular del caso
        Pattern pat = Pattern.compile("^([\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,}))+$");
        Matcher mat = pat.matcher(str);
        return mat.find();
    }

    /**
     * Metodo que muestra el resultado final de la verificacion
     * @param str cadena ingresada
     * @return True si es correcta o False en cualquier otro caso
     */
    public boolean Result(String str) {
        return Caracter(str);
    }

}
